﻿Console.WriteLine("Hello, World!");

var matrix = new[,]
{
    {"9","5","2","14" },
    {"9","5","3","14" },
    {"9","5","4","null" },
    {"9","5","null","null" },
    {"6","null","null","null" },
    {"7","null","null","null" },
    {"8","null","null","null" },
    {"null","null","null","null" },
    {"10","12","13","null" },
    {"11","null","null","null" },
    {"null","null","null","null" },
    {"null","null","null","null" },
    {"null","null","null","null" },
    {"9","5","null","null" },
};
var alphabet = new[] { 'I', 'V', 'X', 'L' };

Console.Write("Input number: ");
var input = Console.ReadLine()!;

while (input != "" && input is not null)
{
    var state = 0;
    var flag = !(input.Length > 7 && input.Length != 0);
    for (var i = 0; i < input.Length && flag; i++)
    {
        var column = Array.FindIndex(alphabet, value => value == input[i]);
        if (alphabet.Contains(input[i]) && matrix[state, column] != "null")
            state = Convert.ToInt32(matrix[state, column]) - 1;
        else flag = false;
    }

    Console.WriteLine(flag ? "Accepted" : "Rejected");
    input = Console.ReadLine();
}
